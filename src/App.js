import React, { Component } from 'react';

import './App.css';

// Getting app components
import Introduction from './components/introduction';
import ButtonGetPlanet from './components/button-getplanet';
import PlanetInfoTable from './components/planetinfo-table';
import Catchphrase from './components/catchphrase';
import LoadingWrapper from './components/loading-wrapper';

// Settings for SWAPI and Lifestar configurations
const Swapi = (uri) => "http://swapi.co/api" + uri;

class App extends Component {

    constructor(props){
        
        super(props);
        this.state = {
            appState: 0,
            planetCount: 0,
            planetInfo: null
        };

        this.getPlanetCount().then((value) => {
            this.setState({ 
                appState: this.state.appState,
                planetCount: value,
                planetInfo: this.state.planetInfo
            });
        });

        this.getPlanet = this.getPlanet.bind(this);
        this.getAppState = this.getAppState.bind(this);
        this.setAppState = this.setAppState.bind(this);
    }

    getPlanetCount() {
        return fetch(Swapi('/planets/'), {method: 'get'})
        .then((response) => {
            return response.json();
        }).then((object) => {
            return object.count;
        }).catch((err) => {
            console.log(err);
        })
    }

    getPlanet(id){
        this.setAppState(1);
        return fetch(Swapi('/planets/' + id), {method: 'get'})
        .then((response) => {
            return response.json();
        }).then((object) => {
            this.setState({
                appState: 2,
                planetCount: this.state.planetCount, 
                planetInfo: object
            });
            console.log(this.state.planetInfo);
        }).catch((err) => {
            console.log(err);
        });
    }

    getAppState(){
        if (this.state.appState === 0){
            return "introduction";
        } else if (this.state.appState === 1) {
            return "interaction__loading"
        } else if (this.state.appState === 2) {
            return "interaction__ended"
        }
    }

    setAppState(appStateCode){
        this.setState({
            appState: appStateCode,
            planetCount: this.state.planetCount, 
            planetInfo: this.state.planetInfo
        });
    }

    render(){
        return (
            <div className={"lifestar__app lifestar__app--" + this.getAppState()}>
                <LoadingWrapper 
                    getAppState={this.getAppState}
                />
                {this.state.appState === 0 ? 
                    <Introduction /> : null
                }
                {
                    this.state.appState === 2 ?
                    <PlanetInfoTable 
                        planetInfo={this.state.planetInfo} 
                    /> : null
                }
                <ButtonGetPlanet 
                    planetCount={this.state.planetCount} 
                    getPlanet={this.getPlanet}
                />
                <Catchphrase />
            </div>
        );
    }
}

export default App;
