import React from 'react';

const Introduction = (props) => {
	return (
		<div className="lifestar__introduction">
			<h1>Lifestar</h1>
			<h2>Information, <br />not so far far away.</h2>
		</div>
	);
}

export default Introduction;