import React from 'react';

const PlanetInfoTable = (props) => {

	// Treating some requested values from API
	const planetInfo = (property) => {
		if (props.planetInfo === null){
			return null;
		} else {
			if (property === "films"){
				return props.planetInfo[property].length;	
			} else if (property === "population"){
				if (!isNaN(parseInt(props.planetInfo[property], 10))){
					return parseInt(props.planetInfo[property], 10).toLocaleString().replace(/,/g, '.');
				}
				return props.planetInfo[property];
			} else {
				return props.planetInfo[property];
			}
		} 
	};

	return  (
		<table className="lifestar__table">
			<caption className="lifestar__caption">{planetInfo("name")}</caption>
			<tbody>
				<tr className="lifestar__tablerow row--population">
					<td className="lifestar__tablecell">Population:</td>
					<td className="lifestar__tablecell">{planetInfo("population")}</td>
				</tr>
				<tr className="lifestar__tablerow row--climate">
					<td className="lifestar__tablecell">Climate:</td>
					<td className="lifestar__tablecell">{planetInfo("climate")}</td>
				</tr>
				<tr className="lifestar__tablerow row--terrain">
					<td className="lifestar__tablecell">Terrain:</td>
					<td className="lifestar__tablecell">{planetInfo("terrain")}</td>
				</tr>
				<tr>
					<td className="lifestar__tablerow row--films" colSpan="2">
						Featured in {planetInfo("films")} 
						{" film" + (planetInfo("films") !== 1 ? "s" : "")}.
					</td>
				</tr>
			</tbody>
		</table>
	);
}

export default PlanetInfoTable;