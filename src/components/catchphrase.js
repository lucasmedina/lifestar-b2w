import React from 'react';

const Catchphrase = (props) => {
	return (
		<p className="lifestar__catchphrase">"Don't be too proud of this technological wonder you've constructed. The ability to get info about a planet is insignificant next to the power of the Force."</p>
	);
}

export default Catchphrase;