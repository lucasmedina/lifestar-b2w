import React, { Component } from 'react';

class ButtonGetPlanet extends Component {

	constructor(props) {
		super(props);
		this.state = {
			currentText: "Get Planet"
		}
		
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(event) {
		// Random number from 1 to max planets
		let id = Math.floor(Math.random() * this.props.planetCount) + 1;

		this.setState({
			currentText: "Next!"
		});
		this.props.getPlanet(id);
	}

	render() {
		return (
			<div className="lifestar__container container--center">
				<button className="lifestar__button lifestar__button--get" onClick={this.handleClick} type="button">{this.state.currentText}</button>
			</div>
		);
	}

}

export default ButtonGetPlanet;