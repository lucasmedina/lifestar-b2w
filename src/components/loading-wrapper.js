import React, { Component } from 'react';

class LoadingWrapper extends Component {
	
	constructor(props){
		super(props);
	}

	render(){
		return (
			<div className={"lifestar__loading-wrapper lifestar__loading-wrapper--" + this.props.getAppState()}>
				<span className="loading__label">Loading...</span>
			</div>
		);
	}
	
}

export default LoadingWrapper;